<?php
	include_once 'includes/db.inc.php';
	include_once 'includes/functions.inc.php';
	$driver = 'mysql';
	if (!isset($_GET['id']) && empty($_GET['id'])) {
		try {
			here:
			$result = $pdo->query("SELECT Code FROM `databases` WHERE IP = '".$_SERVER['REMOTE_ADDR']."' AND Driver = 'mysql'");
			$databases = $result->fetchAll();
			if (count($databases) != 0) {
				foreach ($databases AS $database) {
					try {
						$result = $pdo->query("SELECT COUNT(DISTINCT `table_name`) FROM `information_schema`.`columns` WHERE `table_schema` = 'squiddle_".$database['Code']."'");
						$tableCount = $result->fetchColumn();
						if ($tableCount == 0) {
							$dbCode = $database['Code'];
						}
					} catch (PDOException $e) {
						die("getDB Error: ". $e->getMessage());
					}
				}
				if ($tableCount != 0) {
					$code = substr(str_shuffle(MD5(microtime())), 0, 5);
					$password = pass($code."Password");
					try {
						$pdo->exec("INSERT INTO `databases` (Code, IP, Driver) VALUES ('".$code."', '".$_SERVER['REMOTE_ADDR']."','mysql')");
						$pdo->exec("CREATE DATABASE squiddle_".$code);
						$pdo->exec("CREATE USER '".$code."Username'@'".$host."' IDENTIFIED BY '".$password."';");
						$pdo->exec("GRANT CREATE, DROP, DELETE, INSERT, SELECT, UPDATE, ALTER, INDEX, CREATE TEMPORARY TABLES, SHOW VIEW, CREATE VIEW, TRIGGER ON squiddle_".$code.".* TO '".$code."Username'@'".$host."'");
						$dbCode = $code;
					} catch (PDOException $e) {
						die("createDB Error: ". $e->getMessage());
					}
				}
			} else {
				$code = substr(str_shuffle(MD5(microtime())), 0, 5);
				$password = pass($code."Password");
				try {
					$pdo->exec("INSERT INTO `databases` (Code, IP) VALUES ('".$code."', '".$_SERVER['REMOTE_ADDR']."')");
					$pdo->exec("CREATE DATABASE squiddle_".$code);
					$pdo->exec("CREATE USER '".$code."Username'@'".$host."' IDENTIFIED BY '".$password."';");
					$pdo->exec("GRANT CREATE, DROP, DELETE, INSERT, SELECT, UPDATE, ALTER, INDEX, CREATE TEMPORARY TABLES, SHOW VIEW, CREATE VIEW, TRIGGER ON squiddle_".$code.".* TO '".$code."Username'@'".$host."'");
					$dbCode = $code;
				} catch (PDOException $e) {
					die("createDB Error: ". $e->getMessage());
				}
			}
		} catch (PDOException $e) {
			die("Error: ". $e->getMessage());
		}
	} else {
		$result = $pdo->prepare("SELECT Driver FROM `databases` WHERE Code = :code");
		$result->bindParam(':code', $_GET['id']);
		$result->execute();
		$driver = $result->fetchColumn();
		$dbCode = $_GET['id'];
		$structure = [];
		if ($driver == "mysql") {
			$host1 = 'localhost';
			$db1 = 'squiddle_'.$dbCode;
			$user1 = $dbCode.'Username';
			$pass1 = pass($dbCode."Password");
			$pdo1 = new PDO('mysql:host='.$host1.';dbname='.$db1, $user1, $pass1);
			$pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = $pdo1->query("SHOW TABLES FROM squiddle_".$dbCode);
			$tables = $result->fetchAll();
			if (count($tables) > 0) {
				$i = 0;
				foreach($tables AS $table) {
					$result = $pdo1->query("SHOW COLUMNS FROM ".$table[0]);
					$columnRes = $result->fetchAll();
					$structure[$table[0]] = $columnRes;
					$i++;
				}
			}
		} else {
			goto here;
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>MySQL - Squiddle</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="index,follow" />
		<script src="lib/codemirror.js"></script>
		<link rel="stylesheet" href="lib/codemirror.css">
		<script src="sql/sql.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<body>
		<div id="wrapper">
			<header>
				<h1>Squiddle</h1>
			</header>
			<aside>
			<div class="cur" onclick="changePage(this)">
			<span>MySQL</span></div>
			<div onclick="changePage(this)">
			<span>SQLite</span></div>
			</aside>
			<section>
				<div id="colorBar" style="background-color: #0386b2;"></div>
				<header><span>MySQL Fiddle</span></header>
				<article id="MySQL">
					<div class="left">
						<ul class="tree">
							<li><?php echo "squiddle_".$dbCode ?>
								<?php if (isset($_GET['id']) && !empty($_GET['id'])) { ?>
									<ul>
										<?php foreach($structure as $key => $value) { ?>
												<li><?php echo $key; ?>
													<ul>
														<?php foreach ($value AS $column) { ?>
															<li>
																<?php echo $column['Field']; ?>
															</li>
														<?php } ?>
													</ul>
												</li>
										<?php } ?>
									</ul>
								<?php } ?>
							</li>
						</ul>
					</div>
					<textarea class="right MySQL">--Type MySQL queries here</textarea>
					<span class="submit" onclick="execute(this.previousSibling.previousSibling)">Execute</span>
					<div class="bottom">
						<h1>Results</h1>
					</div>
				</article>
				<article id="SQLite">
					<div class="left">
						<ul class="tree">
						
						</ul>
					</div>
					<textarea class="right SQLite">--Type SQLite queries here</textarea>
					<span class="submit" onclick="execute(this.previousSibling.previousSibling)">Execute</span>
					<div class="bottom">
						<h1>Results</h1>
					</div>
				</article>
			</section>
		</div>
	</body>
</html>
<script>
var firstLoad = true;
var CodeMirrors = [];
var textareas = document.getElementsByClassName('right');
for (var i = 0; i < textareas.length; i++) {
	CodeMirrors.push(CodeMirror.fromTextArea(textareas[i], {
		mode: 'text/x-sql',
		indentWithTabs: true,
		smartIndent: true,
		lineNumbers: true,
		matchBrackets: true
	}));
}
var articles = document.getElementsByTagName("section")[0].getElementsByTagName("article");
for (i = 0; i < articles.length; i++) {
	if (i == 0) {
		articles[i].style.display = 'block';
	} else {
		articles[i].style.display = 'none';
	}
}
function execute(el) {
 	nel = el;
	while(nel.tagName != "ARTICLE") {
		nel = nel.parentNode;
	}
	for (var i = 0; i < CodeMirrors.length; i++) {
		if (CodeMirrors[i].getTextArea().className == "right "+nel.id) {
			var content = CodeMirrors[i].getValue().split("\n");
		}
	}
	dbCode = el.parentNode.childNodes[1].childNodes[1].childNodes[1].textContent.trim().replace("squiddle_", "");
	console.log(el.parentNode.childNodes[1].childNodes);
	driver = el.parentNode.id.toLowerCase();
 	var xmlhttp;
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			if (xmlhttp.responseText.indexOf("Error") == -1) {
				var left = nel.childNodes[1];
				var userResponse = JSON.parse(xmlhttp.responseText.split("<:::>")[0]);
				var structure = JSON.parse(xmlhttp.responseText.split("<:::>")[1]);
				var bottom = nel.childNodes[8]
				bottom.innerHTML = "<h1>Results</h1>";
				if (Object.size(userResponse) > 0) {
					var newTable = document.createElement("table");
					var newThead = document.createElement("thead");
					var newRow = document.createElement("tr");
					userResponse[0].each(function(key,value) {
						var newHeadColumn = document.createElement("th");
						columnName = document.createTextNode(key);
						newHeadColumn.appendChild(columnName);
						newRow.appendChild(newHeadColumn);
					});
					newThead.appendChild(newRow);
					newTable.appendChild(newThead);
					var newTbody = document.createElement("tbody");
					for (var i = 0; i < userResponse.length; i++) {
						var newRow = document.createElement("tr");
						userResponse[i].each(function(key,value) {
							newColumn = document.createElement("td");
							columnValue = document.createTextNode(value);
							newColumn.appendChild(columnValue);
							newRow.appendChild(newColumn);
						});
						newTbody.appendChild(newRow);
					}
					newTable.appendChild(newTbody);
					bottom.appendChild(newTable);
				} else {
					var bottom = el.parentNode.childNodes[8];
					bottom.innerHTML = "<h1>Results</h1>";
					bottom.innerHTML += "<br/><span>MySQL returned an empty result set (i.e. zero rows).</span>";
				}
				if (driver == "mysql") {
					left.innerHTML = "";
					var newTree = document.createElement("UL");
					newTree.setAttribute('class', 'tree');
					var newDb = document.createElement("LI");
					var dbName = document.createTextNode("squiddle_<?php echo $dbCode ?>");
					newDb.appendChild(dbName);
					newTableUL = document.createElement("UL");
					if (Object.size(structure) > 0) {
						structure.each(function(key,value) {
							newTable = document.createElement("LI");
							tableName = document.createTextNode(key);
							newTable.appendChild(tableName);
							newColumnUL = document.createElement("UL");
							if (Object.size(value) > 0) {
								value.each(function(key1,value1) {
									newColumn = document.createElement("LI");
									columnName = document.createTextNode(value1.Field);
									newColumn.appendChild(columnName);
									newColumnUL.appendChild(newColumn);
								});
							}
							newColumnUL.appendChild(newColumn);
							newTable.appendChild(newColumnUL);
							newTableUL.appendChild(newTable);
						});
					}
				} else if(driver == "sqlite") {
					console.log(driver);
					var dbCode = left.childNodes[1].childNodes[1].textContent;
					left.innerHTML = "";
					var newTree = document.createElement("UL");
					newTree.setAttribute('class', 'tree');
					var newDb = document.createElement("LI");
					var dbName = document.createTextNode(dbCode);
					newDb.appendChild(dbName);
					newTableUL = document.createElement("UL");
					if (Object.size(structure) > 0) {
						structure.each(function(key,value) {
							newTable = document.createElement("LI");
							tableName = document.createTextNode(key);
							newTable.appendChild(tableName);
							newColumnUL = document.createElement("UL");
							if (Object.size(value) > 0) {
								value.each(function(key1,value1) {
									newColumn = document.createElement("LI");
									columnName = document.createTextNode(value1[1]);
									newColumn.appendChild(columnName);
									newColumnUL.appendChild(newColumn);
								});
							}
							newColumnUL.appendChild(newColumn);
							newTable.appendChild(newColumnUL);
							newTableUL.appendChild(newTable);
						});
					} 
				}
				newDb.appendChild(newTableUL);
				newTree.appendChild(newDb);
				left.appendChild(newTree);
			} else {
				var bottom = el.parentNode.childNodes[8];
				bottom.innerHTML = "<h1>Results</h1>";
				bottom.innerHTML += "<br/><span class='error'>"+xmlhttp.responseText+"</span>";
			}
			for(var i = 0; i < document.getElementsByClassName("tree").length; i++) {
				document.getElementsByClassName("tree")[i].addEventListener('click',function () {
					var el = event.target;
					var content = el.childNodes[0].textContent.trim();
					nel = el;
					while(nel.tagName != "ARTICLE") {
						nel = nel.parentNode;
					}
					for (var i = 0; i < CodeMirrors.length; i++) {
						if (CodeMirrors[i].getTextArea().className == "right "+nel.id) {
							CodeMirrors[i].replaceRange("`"+content+"`", CodeMirror.Pos(CodeMirrors[i].lastLine()));
						}
					}
				},false);
			}
		}
	}
	xmlhttp.open("GET","runUserQuery.php?dbCode="+dbCode+"&query="+JSON.stringify(content)+"&driver="+driver,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send();
}
function changePage(el) {
	var cur = document.getElementsByClassName("cur")[0];
	cur.className = "";
	document.getElementById(cur.textContent.trim()).style.display = "none";
	el.className = "cur";
	document.getElementById(el.textContent.trim()).style.display = "block";
	document.getElementById(el.textContent.trim()).parentNode.childNodes[3].childNodes[0].textContent = el.textContent+" Fiddle";
	document.getElementById('colorBar').style.backgroundColor = stringToColor(el.textContent.trim());
	document.title = el.textContent.trim()+" - Squidle";
	if (document.getElementById(el.textContent.trim()).childNodes[1].childNodes[1].childNodes.length <= 1) {
		var xmlhttp;
		xmlhttp=new XMLHttpRequest();
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				if (xmlhttp.responseText.indexOf("<:::>") != -1) {
				
					var tree = document.getElementById(el.textContent.trim()).childNodes[1].childNodes[1];
					var newDb = document.createElement("LI");
					var dbName = document.createTextNode(xmlhttp.responseText.split("<:::>")[0]);
					newDb.appendChild(dbName);
					var structure = JSON.parse(xmlhttp.responseText.split("<:::>")[1]);
					console.log(structure);
					newTableUL = document.createElement("UL");
					if (Object.size(structure) > 0) {
						structure.each(function(key,value) {
						console.log(value);
							newTable = document.createElement("LI");
							tableName = document.createTextNode(key);
							newTable.appendChild(tableName);
							newColumnUL = document.createElement("UL");
							if (Object.size(value) > 0) {
								value.each(function(key1,value1) {
									newColumn = document.createElement("LI");
									columnName = document.createTextNode(value1[1]);
									newColumn.appendChild(columnName);
									newColumnUL.appendChild(newColumn);
								});
							}
							newColumnUL.appendChild(newColumn);
							newTable.appendChild(newColumnUL);
							newTableUL.appendChild(newTable);
						});
					}
					newDb.appendChild(newTableUL);
					tree.appendChild(newDb);
				} else {
					var tree = document.getElementById(el.textContent.trim()).childNodes[1].childNodes[1];
					var newDb = document.createElement("LI");
					var dbName = document.createTextNode(xmlhttp.responseText);
					newDb.appendChild(dbName);
					tree.appendChild(newDb);
				}
			}
		}
		<?php if (isset($_GET['id'])) {	?>
			<?php if ($driver == "mysql") { ?>
				xmlhttp.open("GET","changePage.php?driver="+el.textContent.trim().toLowerCase(),true);
			<?php } else { ?>
				xmlhttp.open("GET","changePage.php?driver="+el.textContent.trim().toLowerCase()+"&id=<?php echo $_GET['id']; ?>",true);
			<?php } ?>
		<?php } else { ?>
			xmlhttp.open("GET","changePage.php?driver="+el.textContent.trim().toLowerCase(),true);
		<?php } ?>
		xmlhttp.send();
		firstLoad = false;
	}
}
function stringToColor(str){
  var hash = 0;
  for(var i=0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 3) - hash);
  }
  var color = Math.abs(hash).toString(16).substring(0, 6);

  return "#" + '000000'.substring(0, 6 - color.length) + color;
}

var timeoutID;
var isInactive = false;
 
function setup() {
    this.addEventListener("mousemove", resetTimer, false);
    this.addEventListener("mousedown", resetTimer, false);
    this.addEventListener("keypress", resetTimer, false);
    this.addEventListener("DOMMouseScroll", resetTimer, false);
    this.addEventListener("mousewheel", resetTimer, false);
    this.addEventListener("touchmove", resetTimer, false);
    this.addEventListener("MSPointerMove", resetTimer, false);
    startTimer();
}
setup();
 
function startTimer() {
    timeoutID = window.setTimeout(goInactive, 60000);
}
 
function resetTimer(e) {
    window.clearTimeout(timeoutID);
    goActive();
}
var audio = new Audio('squiddle.mp3');
function goInactive() {
	if (!isInactive) {
		//audio.play();
		isInactive = true;
	}
}
 
function goActive() {
    isInactive = false;
	audio.pause();
    startTimer();
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

function addText(el, event) {
	var content = event.target.childNodes[0].textContent.trim();
	nel = el;
	while(nel.tagName != "ARTICLE") {
		nel = nel.parentNode;
	}
	for (var i = 0; i < CodeMirrors.length; i++) {
		if (CodeMirrors[i].getTextArea().className == "right "+nel.id) {
			CodeMirrors[i].replaceRange("`"+content+"`", CodeMirror.Pos(CodeMirrors[i].lastLine()));
		}
	}
}

function cleanArray(arr) {
	var narr = []
	for(var i = 0; i < arr.length; i++) {
		if (encodeURI(arr[i]) != "%C2%A0") {
			narr.push(arr[i]);
		}
	}
	return narr;
}
Object.prototype.each = function(f) {
    var obj = this
    Object.keys(obj).forEach( function(key) { 
        f( key , obj[key] ) 
    });
}
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
for(var i = 0; i < document.getElementsByClassName("tree").length; i++) {
	document.getElementsByClassName("tree")[i].addEventListener('click',function () {
		var el = event.target;
		var content = el.childNodes[0].textContent.trim();
		nel = el;
		while(nel.tagName != "ARTICLE") {
			nel = nel.parentNode;
		}
		for (var i = 0; i < CodeMirrors.length; i++) {
			if (CodeMirrors[i].getTextArea().className == "right "+nel.id) {
				CodeMirrors[i].replaceRange("`"+content+"`", CodeMirror.Pos(CodeMirrors[i].lastLine()));
			}
		}
	},false);
}
if ("<?php echo $driver ?>" == "sqlite") {
	var navs = document.getElementsByTagName("aside")[0].getElementsByTagName("div");
	for (var i = 0; i < navs.length; i++) {
		if (navs[i].textContent.toLowerCase().trim() == "sqlite") {
			changePage(navs[i]);
		}
	}
}
</script>