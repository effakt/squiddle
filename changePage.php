<?php
	include 'includes/db.inc.php';
	$result = $pdo->prepare("SELECT Code FROM `databases` WHERE IP = '".$_SERVER['REMOTE_ADDR']."' AND Driver = :driver");
	$result->bindParam(':driver', $_GET['driver']);
	$result->execute();
	$databases = $result->fetchAll();
	if (isset($_GET['id'])) {
		if ($_GET['driver'] == 'sqlite') {
			$file_db = new PDO('sqlite:squiddle/'.$_GET['id'].'.db');
			$file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = $file_db->query("SELECT name FROM sqlite_master WHERE type='table';");
			$tables = $result->fetchAll();
			if (count($tables) > 0) {
				$i = 0;
				foreach ($tables AS $table) {
					$result = $file_db->query("PRAGMA table_info(".$table[0].");");
					$columnRes = $result->fetchAll();
					$structure[$table[0]] = $columnRes;
					$i++;
				}
			}
		}
		die($_GET['id']."<:::>".json_encode($structure));
	}
	if (count($databases) != 0) {
		foreach ($databases AS $database) {
			if ($_GET['driver'] == 'sqlite') {
				$file_db = new PDO('sqlite:squiddle/'.$database['Code'].'.db');
				$file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$result = $file_db->query("SELECT COUNT(*) FROM sqlite_master WHERE type='table';");
				$tableCount = $result->fetchColumn();
				if ($tableCount == 0) {
					$dbCode = $database['Code'];
					continue;
				}
			}
		}
		if ($tableCount != 0) {
			$code = substr(str_shuffle(MD5(microtime())), 0, 5);
			if ($_GET['driver'] == 'sqlite') {
				try {
					$pdo->exec("INSERT INTO `databases` (Code, IP, Driver) VALUES ('".$code."', '".$_SERVER['REMOTE_ADDR']."','sqlite')");
					$file_db = new PDO('sqlite:squiddle/'.$code.'.db');
					$file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$dbCode = $code;
				} catch (PDOException $e) {
					die("createDB Error: ". $e->getMessage());
				}
			}
		}
	} else {
		$code = substr(str_shuffle(MD5(microtime())), 0, 5);
		if ($_GET['driver'] == 'sqlite') {
			try {
				$pdo->exec("INSERT INTO `databases` (Code, IP, Driver) VALUES ('".$code."', '".$_SERVER['REMOTE_ADDR']."','sqlite')");
				$file_db = new PDO('sqlite:squiddle/'.$code.'.db');
				$file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$dbCode = $code;
			} catch (PDOException $e) {
				die("createDB Error: ". $e->getMessage());
			}
		}
	}
	echo $dbCode;
?>