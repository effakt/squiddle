<?php
	include 'includes/functions.inc.php';
	include 'includes/db.inc.php';
	if (isset($_GET['dbCode']) && !empty($_GET['dbCode']) && isset($_GET['query']) && !empty($_GET['query']) && isset($_GET['driver']) && !empty($_GET['driver'])) {
		$driver = $_GET['driver'];
		$userResult = [];
		$dbCode = $_GET['dbCode'];
		$query = json_decode($_GET['query']);
		$query = implode($query);
		$structure = [];
		if ($driver == "mysql") {
			$host1 = 'localhost';
			$db1 = 'squiddle_'.$dbCode;
			$user1 = $dbCode.'Username';
			$pass1 = pass($dbCode."Password");
			$pdo1 = new PDO('mysql:host='.$host1.';dbname='.$db1, $user1, $pass1);
			$pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			try {
				$results = $pdo->query("SELECT * FROM banned_commands");
				$banned_commands = $results->fetchAll();
				foreach($banned_commands AS $bancom) {
					if (preg_match($bancom['Command'], $query, $matches)) {
						die("Error: ".$matches[0]." is not allowed to be used");
					}
				}
				$result = $pdo1->query($query);
				$userResult = $result->fetchAll(PDO::FETCH_ASSOC);
				$result = $pdo1->query("SHOW TABLES FROM squiddle_".$dbCode);
				$tables = $result->fetchAll();
				if (count($tables) > 0) {
					$i = 0;
					foreach($tables AS $table) {
						$result = $pdo1->query("SHOW COLUMNS FROM ".$table[0]);
						$columnRes = $result->fetchAll();
						$structure[$table[0]] = $columnRes;
						$i++;
					}
				}
				//var_export($structure);
			} catch(PDOException $e) {
				if ($e->getCode() == "HY000") {
					$userResult = [];
					$result = $pdo1->query("SHOW TABLES FROM squiddle_".$dbCode);
					$tables = $result->fetchAll();
					if (count($tables) > 0) {
						$structure = [];
						$i = 0;
						foreach($tables AS $table) {
							$result = $pdo1->query("SHOW COLUMNS FROM ".$table[0]);
							$columnRes = $result->fetchAll();
							$structure[$table[0]] = $columnRes;
							$i++;
						}
					}
				} else {
					die("Error: ".$pdo1->errorInfo()[2]);
				}
			}
			echo json_encode($userResult)."<:::>".json_encode($structure);
		} elseif ($driver == "sqlite") {
			$file_db = new PDO('sqlite:squiddle/'.$dbCode.'.db');
			$file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			try {
				$results = $pdo->query("SELECT * FROM banned_commands");
				$banned_commands = $results->fetchAll();
				foreach($banned_commands AS $bancom) {
					if (preg_match($bancom['Command'], $query, $matches)) {
						die("Error: ".$matches[0]." is not allowed to be used");
					}
				}
				//run user query
				$result = $file_db->query($query);
				$userResult = $result->fetchAll(PDO::FETCH_ASSOC);
				
				//get structure
				$result = $file_db->query("SELECT name FROM sqlite_master WHERE type='table';");
				$tables = $result->fetchAll();
				if (count($tables) > 0) {
					$i = 0;
					foreach ($tables AS $table) {
						$result = $file_db->query("PRAGMA table_info(".$table[0].");");
						$columnRes = $result->fetchAll();
						$structure[$table[0]] = $columnRes;
						$i++;
					}
				}
			} catch(PDOException $e) {
				if ($e->getCode() == "HY000") {
					if (count($tables) > 0) {
					$i = 0;
						foreach ($tables AS $table) {
							$result = $file_db->query("PRAGMA table_info(".$table[0].");");
							$columnRes = $result->fetchAll();
							$structure[$table[0]] = $columnRes;
							$i++;
						}
					}
				} else {
					die("Error: ".$pdo1->errorInfo()[2]);
				}
			}
			echo json_encode($userResult)."<:::>".json_encode($structure);
		}
	}
?>