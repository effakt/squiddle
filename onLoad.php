<?php
	include_once 'includes/db.inc.php';
	include_once 'includes/functions.inc.php';
	try {
		start:
		$result = $pdo->query("SELECT COUNT(*), Code FROM `databases` WHERE IP = '".$_SERVER['REMOTE_ADDR']."'");
		$databases = $result->fetchAll();
		if (count($databases) != 0) {
			foreach ($databases AS $database) {
				try {
					$result = $pdo->query("SELECT COUNT(DISTINCT `table_name`) FROM `information_schema`.`columns` WHERE `table_schema` = 'squiddle_".$database['Code']."'");
					$tableCount = $result->fetchColumn();
					if ($tableCount != 0) {
						$dbCode = $database['Code'];
						die("Exists ".$dbCode);
					}
				} catch (PDOException $e) {
					die("getDB Error: ". $e->getMessage());
				}
			}
			$code = substr(str_shuffle(MD5(microtime())), 0, 5);
			$password = pass($code."Password");
			try {
				$pdo->exec("INSERT INTO `databases` (Code, IP) VALUES ('".$code."', '".$_SERVER['REMOTE_ADDR']."')");
				$pdo->exec("INSERT INTO `users` (DbCode, Username, Password) VALUES ('".$code."', '".$code."Username', '".$password."')");
				$pdo->exec("CREATE DATABASE squiddle_".$code);
				$pdo->exec("CREATE USER '".$code."Username'@'".$host."' IDENTIFIED BY '".$password."';");
				$pdo->exec("GRANT CREATE, DROP, DELETE, INSERT, SELECT, UPDATE, ALTER, INDEX, CREATE TEMPORARY TABLES, SHOW VIEW, CREATE VIEW, TRIGGER ON squiddle_".$code.".* TO '".$code."Username'@'".$host."'");
				die("Created ".$code);
			} catch (PDOException $e) {
				die("createDB Error: ". $e->getMessage());
			}
		}
	} catch (PDOException $e) {
		die("Error: ". $e->getMessage());
	}
	
?>